#METODOLOGIA DE LA PROGRAMACION
#TPN°4-Punto2

def Menu():
    print(" # # # FERRETERIA # # #")
    print("1) Registrar productos")
    print("2) Mostrar listado")
    print("3) Stock dentro de un intervalo")
    print("4) Sumar X a un stock menor a Y")
    print("5) Eliminar stock (0)")
    print("6) Salir")
    eleccion = int(input("Elija una opcion: "))
    while not ((eleccion >= 1) and (eleccion <= 6)):
        eleccion = int(input("Elija una opcion: "))
    return eleccion

def cargarDesc():
    desc = input("Descripcion: ")
    while desc == " ":
        print("Por favor complete el campo!")
        desc = input("Descripcion: ")
    return desc

def cargarPrecio():
    precio = input("Precio: ")
    while precio == " ":
        print("Por favor complet el campo!")
        precio = input("Precio: ")
    precio = float(precio)
    return precio

def cargarStock():
    st = input("Stock: ")
    while st == " ":
        print("Por favor complete el campo!")
        st = input("Stock: ")
    st = int(st)
    return st

def validar(x):
    if x < 0:
        return True
    else:
        return False

def registrarProductos(productos):
    resp = "s"
    while resp == "s" or resp == "S":
        codigo = input("Codigo: ")
        while codigo == " ":
            print("Por favor complete el campo!")
            codigo = input("Codigo: ")
        codigo = int(codigo)
        if codigo not in productos:
            desc = cargarDesc()
            precio = cargarPrecio()
            st = cargarStock()
            if not (validar(precio)) and not (validar(st)):
                productos[codigo] = [desc,precio,st]
            else:
                print("El precio y el stock deben ser positivos!")
        else:
            print("El codigo ya existe!")
        resp = input("Continuar? s/n: ")
    return productos

def mostrarProductos(x):
    for clave,valor in x.items():
        print(f"Codigo: {clave} Descripcion: {valor[0]} Precio: {valor[1]} Stock: {valor[2]}")

def stock(x,d,h):
    for clave,valor in x.items():
        if valor[2] >= d and valor[2] <= h:
            print(f"Codigo: {clave} Descripcion: {valor[0]} Precio: {valor[1]} Stock: {valor[2]}")

def incremento(d,x,y):
    for clave,valor in d.items():
        if valor[2] < y:
            valor[2] = valor[2] + x

def verificarStock(x):
    aux = False
    for clave,valor in x.items():
        if valor[2] == 0:
            aux = True
    return aux

def eliminarStock(x):
    for clave,valor in list(x.items()):
         if valor[2] == 0:
             del x[clave]

#Modulo Principal
productos = {}
opcion = 0
while opcion != 6:
    opcion = Menu()
    if opcion == 1:
        print("# REGISTRAR PRODUCTOS #")
        productos = registrarProductos(productos)
    elif opcion >= 2 and opcion <= 5:
        if productos != {}:
            if opcion == 2:
                print("# PRODUCTOS #")
                mostrarProductos(productos)
            elif opcion == 3:
                d = int(input("Stock desde: "))
                h = int(input("Stock hasta: "))
                print(f"# Stock [{d},{h}]")
                stock(productos,d,h)
            elif opcion == 4:
                print("# Incrementar Stock #")
                x = int(input("Ingrese el valor de X: "))
                y = int(input("Ingrese el valor de Y: "))
                incremento(productos,x,y)
                print("# Stock Actualizado #")
                mostrarProductos(productos)
            elif opcion == 5:
                if verificarStock(productos):
                    eliminarStock(productos)
                    print("Se eliminaron correctamente!")
                else:
                    print("No se registraron productos con stock = 0")
        else:
            print("No se registraron productos!")
    elif opcion == 6:
        print("FIN DEL PROGRAMA")